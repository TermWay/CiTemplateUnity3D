# CI Status
[![pipeline status](https://gitlab.com/TermWay/%CI_PROJECT_NAME/badges/master/pipeline.svg)](https://gitlab.com/TermWay/%CI_PROJECT_NAME/commits/master)

# All-in-one script file
One MonoBehaviour in one concatenated file:

- [C# file](https://termway.gitlab.io/%CI_PROJECT_NAME/%CI_PROJECT_NAME.cs)
- [Zip file](https://gitlab.com/TermWay/%CI_PROJECT_NAME/-/jobs/artifacts/master/download?job=generate_single_script_file)
- [Snippet page](https://gitlab.com/TermWay/%CI_PROJECT_NAME/snippets/%SNIPPET_ID)

# Unity package
The unitypackage file contains a sample scene and scripts in a more organized way. It also contains a zip with the all-in-script.

- [Unitypackage file](https://termway.gitlab.io/%CI_PROJECT_NAME/%CI_PROJECT_NAME.unitypackage)
- [Zip file](https://gitlab.com/TermWay/%CI_PROJECT_NAME/-/jobs/artifacts/master/download?job=export_unitypackage)

# WebGL demo link
A WebGL demo is available on the following link:  

- [Web page](https://termway.gitlab.io/%CI_PROJECT_NAME/)
- [Zip file](https://gitlab.com/TermWay/%CI_PROJECT_NAME/-/jobs/artifacts/master/download?job=build_WebGL_Unity_2018)

# %CI_PROJECT_NAME

A sentence to describe the project.
Key features:
* Most important
* ...
* Least important

# Supported Unity3D versions

* Latest stable at the moment (2019.2.2)
* Unity 2018.4 LTS
* Unity 2017.4 LTS
* Unity 5.6

# License
[XXX License](LICENSE.md) (c) 2019 Termway.

# See also
References