﻿using UnityEditor;
using UnityEngine.SceneManagement;
using UnityEngine;

using System;
using System.IO;
using System.Linq;
using UnityEditor.SceneManagement;

using Termway.%CI_PROJECT_NAME;

/// <summary>
/// Create a new scene with a sample script.
/// </summary>
static class CreateScene
{
    [MenuItem("Scene/Create New Scene")]
    static void CreateNewScene()
    {
        Console.WriteLine("> Create sample scene.");
        Scene newScene = EditorSceneManager.NewScene(NewSceneSetup.DefaultGameObjects);

        GameObject go = new GameObject("%CI_PROJECT_NAME");
        go.AddComponent<%CI_PROJECT_NAME>();
        if (EditorSceneManager.SaveScene(newScene, "Assets/%CI_PROJECT_NAME/Samples/SampleScene.unity"))
            Console.WriteLine("> Scene has been saved.");
        else
            Console.WriteLine("> Scene has not been saved.");
    }
}
