# CI Status
[![pipeline status](https://gitlab.com/TermWay/citemplateunity3d/badges/master/pipeline.svg)](https://gitlab.com/TermWay/citemplateunity3d/commits/master)

# All-in-one script file
One MonoBehaviour in one concatenated file:

- [C# file](https://termway.gitlab.io/citemplateunity3d/CiTemplateUnity3D.cs)
- [Zip file](https://gitlab.com/TermWay/citemplateunity3d/-/jobs/artifacts/master/download?job=generate_single_script_file)
- [Snippet page](https://gitlab.com/TermWay/citemplateunity3d/snippets/1903450)

# Unity package
The unitypackage file contains a sample scene and scripts in a more organized way. It also contains a zip with the all-in-script.

- [Unitypackage file](https://termway.gitlab.io/citemplateunity3d/CiTemplateUnity3D.unitypackage)
- [Zip file](https://gitlab.com/TermWay/citemplateunity3d/-/jobs/artifacts/master/download?job=export_unitypackage)

# WebGL demo link
A WebGL demo is available on the following link:  

- [Web page](https://termway.gitlab.io/citemplateunity3d/)
- [Zip file](https://gitlab.com/TermWay/citemplateunity3d/-/jobs/artifacts/master/download?job=build_WebGL_Unity_2018)

# CiTemplateUnity3D

CI Template for Unity3D. 
* Get Unity3D licenses
* Build on different Unity3D versions
* Run Unity3D tests
* Generate all-in-one script file and unity package and host them on a gitlab page
* Update a snippet with the all-in-one script

# Compatible Unity versions 

* Latest stable at the moment (2019.2.2)
* Unity 2018.4 LTS
* Unity 2017.4 LTS
* Unity 5.6

# Variable to define in Gitlab CI/CD

Variables are necessary to make the project work. To get the licenses content and store it.
To set variable in gitlab : Settings > CI / CD > Variable. There are used in ci scripts folder.

## To recover license

To recover the license of a specific Unity. 

* **UNITY_MAIL** (unity username)
* **UNITY_PASSWORD** (masked would be better).
You can create a temporary account on Unity for extra security. Check the masked variable [constraints](https://gitlab.com/help/ci/variables/README#masked-variables) before creating a new account.

## For different Unity3D version

Using license file seems to be the only current way to connect the ci on Unity.
So you need one variable per version
On this project :
* **UNITY_LATEST_LICENSE_XML**
* **UNITY_2018_LICENSE_XML**
* **UNITY_2017_LICENSE_XML**
* **UNITY_5_LICENSE_XML**

To recover the value. Trigger the manual job to get the license content, you need to copy/paste the xml value into a .alf file then go to the [manual activation page](https://license.unity3d.com/manual) to activate Unity manually.

## To factorize and add flexibility

Can be specified in the .gitlab-ci.yml file :
* **CI_PATH** ci folder used. When this project is used as submodule.

## To update a snippet

* **PRIVATE_TOKEN** Token used for gitlab API access (https://gitlab.com/profile/personal_access_tokens)
* **SNIPPET_ID** id of a [project snippet](https://docs.gitlab.com/ee/api/project_snippets.html)

# License
[MIT License](LICENSE.md) (c) 2019 Termway.

# gitlab-ci
Thanks to [gableroux](https://gitlab.com/gableroux) for providing the docker image and the gitlab-ci-example base project.
More information about CI and Unity can be found on his page project https://gitlab.com/gableroux/unity3d-gitlab-ci-example. 
