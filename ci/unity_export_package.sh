#!/usr/bin/env bash

apt-get update; apt-get install zip -y

echo "Export package on $UNITY_VERSION"
FILE_PATH="$CI_PROJECT_NAME".unitypackage
export BUILD_PATH
BUILD_PATH=Builds/
export FULL_PATH
FULL_PATH="$BUILD_PATH$FILE_PATH"
export PACKAGE_PATH
PACKAGE_PATH=Assets/"$CI_PROJECT_NAME"
mkdir -p "$BUILD_PATH"

ls -al
# Problem with case when folder is create with right case it become buggy.
ls -al "$(pwd)/$CI_PROJECT_NAME"

# Zip the single script file into the unitypackage.
zip "$CI_PROJECT_NAME/Assets/$CI_PROJECT_NAME/$CI_PROJECT_NAME.cs.zip" "$CI_PROJECT_NAME.cs"

# Add the readme as documentation
cp README.txt "$CI_PROJECT_NAME/Assets/$CI_PROJECT_NAME/README.txt"

ls -al "$(pwd)/$CI_PROJECT_NAME/Assets"


${UNITY_EXECUTABLE:-xvfb-run --auto-servernum --server-args='-screen 0 640x480x24' /opt/Unity/Editor/Unity} \
  -projectPath "$(pwd)/$CI_PROJECT_NAME" \
  -quit \
  -batchmode \
  -exportPackage "$PACKAGE_PATH" "../$FULL_PATH" \
  -logFile /dev/stdout 

UNITY_EXIT_CODE="$?"

if [ $UNITY_EXIT_CODE -eq 0 ]; then
  echo "Run succeeded, no failures occurred";
elif [ $UNITY_EXIT_CODE -eq 1 ]; then
  echo "Exception occurs during execution of script code, asset server updates fail or other operations fail.";
elif [ $UNITY_EXIT_CODE -eq 2 ]; then
  echo "Run succeeded, some tests failed";
elif [ $UNITY_EXIT_CODE -eq 3 ]; then
  echo "Run failure (other failure)";
else
  echo "Unexpected exit code $UNITY_EXIT_CODE";
fi

ls -la "$BUILD_PATH"
[ -n "$(ls -A "$BUILD_PATH")" ] # fail job if build folder is empty
exit 