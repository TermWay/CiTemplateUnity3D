#!/usr/bin/env bash

apt-get update; apt-get install curl -y

pwd
ls -l
ls -l Builds

generated_file_name="$CI_PROJECT_NAME.cs"
cat "$generated_file_name"

#Escape double quote
sed 's/"/\\"/g' "$generated_file_name" > generated_file_escaped_content
cat generated_file_escaped_content

#Replace newline per \n because of json limitation
awk '{printf "%s\\n", $0}' generated_file_escaped_content > generated_file_escaped_content_newline
cat generated_file_escaped_content_newline

escaped_content=$(cat generated_file_escaped_content_newline)
echo "$escaped_content"

#Generate snippet content
cat > snippet.json <<- EOM
{
    "title" : "$CI_PROJECT_NAME all-in-one script file.", 
    "description" : "$CI_PROJECT_NAME all-in-one script file.", 
    "file_name" : "$CI_PROJECT_NAME.cs", 
    "code" : "$escaped_content", 
    "visibility" : "public" 
} 
EOM

cat snippet.json

# Update project snipet to gitlab ( https://docs.gitlab.com/ee/api/project_snippets.html )
curl --request PUT https://gitlab.com/api/v4/projects/"$CI_PROJECT_ID"/snippets/"$SNIPPET_ID" \
     --header "PRIVATE-TOKEN: $PRIVATE_TOKEN" \
     --header "Content-Type: application/json" \
     -d @snippet.json