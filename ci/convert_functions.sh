#!/usr/bin/env bash

# convert_markdown_to_unityAssetsStore README.md README_metadata_description.html
# Title, newline, bold and link.
# $1 input file name
# $2 output file name
convert_markdown_to_unity_assets_store_metadata_description() 
{
    cp "$1" "$2"
    sed -i -r -e 's:^#+(.+):<strong>\1 </strong>:i' \
            -e 's:$: <br>:' \
            -e 's:\*\*(.+)\*\*:<strong>\1</strong>:g' \
            -e 's:\[(.*)\]\((.*)\):<a href="\2">\1</a>:' "$2"
}

