#!/usr/bin/env bash
# The MIT License (MIT) Copyright (c) 2017 Gabriel Le Breton

echo "Get license content"

timeout 300s xvfb-run --auto-servernum --server-args='-screen 0 640x480x24' \
  /opt/Unity/Editor/Unity \
    -logFile /dev/stdout \
    -batchmode \
    -createManualActivationFile \
    -username "$UNITY_MAIL" -password "$UNITY_PASSWORD" | tee unity.log
    
#grep -oPm1 "<?xml(.*)<\/root>" unity.log > unity_license.alf
ls -al ./*.alf

exit $?