#!/usr/bin/env bash

# $1 input file name
# $2 concat output file name
concat_and_comment() 
{
    {
        echo "/* $1" 
        cat "$1" 
        echo "*/" 
        echo 
        echo 
    } >> "$2"
}

# $1 input folder name
# $2 output file name
concat_folder()
{
   echo "$1"
   for f in "$1"/*.cs  
   do 
   (
        echo "/// $f"
        cat "${f}"
        echo
        echo
   ) >> "$2"
   done
}

# $1 input folder name
# $2 output file name
concat_folder_recursive()
{
    find "$1" -name "*.cs"    \
        -exec echo "/// {}" \; \
        -exec cat {} \;       \
        -exec echo \;         \
        -exec echo \;  >> "$2"
}

# $1 input file name
# $2 output file name
concat_file_newline()
{
    {
        echo "/// $1"
        cat "$1" 
        echo 
        echo
    } >> "$2"
}

# $1 input file name
# $2 pattern
split_file()
{
    csplit --digits=1 -f "$1" "$1" "/$2/+1" "{*}"
}

# $1 input file name
# $2 output file name
# $3 begin text included
# $4 end text excluded
cut_file_begin_end()
{
    begin_line_number=$(awk "/$3/{print NR}" "$1")
    end_line_number=$(awk "/$4/{print NR}" "$1")
    total_line_number=$(< "$1" wc -l)
    echo "Cut the $1 file ($total_line_number lines) into $2 file between line $begin_line_number and $end_line_number ($((total_line_number - end_line_number + 1)))"
    tail -n +"$begin_line_number" "$1" | head -n -$((total_line_number - end_line_number + 1)) > "$2"
}