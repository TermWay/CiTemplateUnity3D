#!/usr/bin/env bash

# Info can be found here https://wiki.bash-hackers.org/scripting/debuggingtips

set -e # Exit on error
# set -v # Print commands before subtitute
set -x # Print commands after subtitute

# Print line of command (need set -x)
export PS4='\e[1;34m (${BASH_SOURCE}:${LINENO}): ${FUNCNAME[0]:+${FUNCNAME[0]}():} \e[0m'

# No working at the moment 10/2019
ls --color=auto
echo "test color" | grep --color=auto "color"