#!/usr/bin/env bash

# The MIT License (MIT) Copyright (c) 2017 Gabriel Le Breton
echo "Building for $BUILD_TARGET on $UNITY_VERSION"

export BUILD_PATH
BUILD_PATH=$(pwd)/Builds/"$BUILD_TARGET"/
mkdir -p "$BUILD_PATH"

ls -al
# Problem with case when folder is create with right case it become buggy.
ls -al "$(pwd)/$CI_PROJECT_NAME"

${UNITY_EXECUTABLE:-xvfb-run --auto-servernum --server-args='-screen 0 640x480x24' /opt/Unity/Editor/Unity} \
  -projectPath "$(pwd)/$CI_PROJECT_NAME" \
  -quit \
  -batchmode \
  -buildTarget "$BUILD_TARGET" \
  -customBuildTarget "$BUILD_TARGET" \
  -customBuildName "$CI_PROJECT_NAME" \
  -customBuildPath "$BUILD_PATH" \
  -customBuildOptions AcceptExternalModificationsToPlayer \
  -executeMethod BuildCommand.PerformBuild \
  -logFile /dev/stdout

UNITY_EXIT_CODE=$?

if [ $UNITY_EXIT_CODE -eq 0 ]; then
  echo "Run succeeded, no failures occurred";
elif [ $UNITY_EXIT_CODE -eq 1 ]; then
  echo "Exception occurs during execution of script code, asset server updates fail or other operations fail.";
elif [ $UNITY_EXIT_CODE -eq 2 ]; then
  echo "Run succeeded, some tests failed";
elif [ $UNITY_EXIT_CODE -eq 3 ]; then
  echo "Run failure (other failure)";
else
  echo "Unexpected exit code $UNITY_EXIT_CODE";
fi

ls -la "$BUILD_PATH"
[ -n "$(ls -A "$BUILD_PATH")" ] # fail job if build folder is empty
exit 