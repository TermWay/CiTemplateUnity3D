#!/usr/bin/env bash

# The MIT License (MIT) Copyright (c) 2017 Gabriel Le Breton

set -e
set -x

mkdir -p /root/.cache/unity3d
mkdir -p /root/.local/share/unity3d/Unity/

set +x

unity_license_key="UNITY_${UNITY_VERSION}_LICENSE_XML"
unity_license_xml="$unity_license_key"
echo "Writing $unity_license_xml to license file /root/.local/share/unity3d/Unity/Unity_lic.ulf"
echo "${!unity_license_xml}" | tr -d '\r' > /root/.local/share/unity3d/Unity/Unity_lic.ulf

echo "Writing $unity_license_xml to license file /root/.local/share/unity3d/Unity/Unity_v5.x.ulf"
echo "${!unity_license_xml}" | tr -d '\r' > /root/.local/share/unity3d/Unity/Unity_v5.x.ulf
