#!/usr/bin/env bash

# The MIT License (MIT) Copyright (c) 2017 Gabriel Le Breton

echo "Testing for $TEST_PLATFORM"

timeout 600s ${UNITY_EXECUTABLE:-xvfb-run --auto-servernum --server-args='-screen 0 640x480x24' /opt/Unity/Editor/Unity} \
  -projectPath "$(pwd)/$CI_PROJECT_NAME/" \
  -batchmode \
  -runTests \
  -nographics \
  -testPlatform "$TEST_PLATFORM" \
  -testResults "$(pwd)/$TEST_PLATFORM-results.xml" \
  -logFile /dev/stdout

UNITY_EXIT_CODE=$?

if [ $UNITY_EXIT_CODE -eq 0 ]; then
  echo "Run succeeded, no failures occurred";
elif [ $UNITY_EXIT_CODE -eq 1 ]; then
  echo "Exception occurs during execution of script code, asset server updates fail or other operations fail.";
elif [ $UNITY_EXIT_CODE -eq 2 ]; then
  echo "Run succeeded, some tests failed";
elif [ $UNITY_EXIT_CODE -eq 3 ]; then
  echo "Run failure (other failure)";
else
  echo "Unexpected exit code $UNITY_EXIT_CODE";
fi

< "$(pwd)/$TEST_PLATFORM-results.xml" grep test-run | grep Passed
exit 