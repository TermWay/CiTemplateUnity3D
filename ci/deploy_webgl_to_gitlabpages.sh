#!/usr/bin/env bash

pwd
ls -al
ls -al Builds

mkdir public

mv "$CI_PROJECT_NAME".cs public/"$CI_PROJECT_NAME".cs
mv Builds/"$CI_PROJECT_NAME".unitypackage public/"$CI_PROJECT_NAME".unitypackage
mv Builds/WebGL/"$CI_PROJECT_NAME"/* public/

ls -al public
