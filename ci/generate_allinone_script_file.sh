#!/usr/bin/env bash

pwd
ls -al

generated_file="$CI_PROJECT_NAME".cs
path="$CI_PROJECT_NAME"/Assets/"$CI_PROJECT_NAME"/Scripts

echo "/// Generated all-in-one script file $CI_PROJECT_NAME.cs the $(date) from $CI_JOB_URL." > "$generated_file"

concat_and_comment LICENSE.md "$generated_file"
cut_file_begin_end README.md README.txt "# $CI_PROJECT_NAME" "# License"
concat_and_comment README.txt "$generated_file"

concat_file_newline "$path/$CI_PROJECT_NAME".cs "$generated_file"

ls -d "$PWD/$generated_file"
 
cat "$generated_file"