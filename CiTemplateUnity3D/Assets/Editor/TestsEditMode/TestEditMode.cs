﻿using NUnit.Framework;

using System.Collections;
using System.Collections.Generic;

using UnityEngine.TestTools;

namespace Tests
{
    public class TestEditMode
    {
        [Test]
        public void IsTrue()
        {
            Assert.IsTrue(true);
        }

        [Test]
        public void IsFalse()
        {
            Assert.IsFalse(false);
        }

        [Test]
        public void IsEmpty()
        {
            Assert.IsEmpty("");
            Assert.IsEmpty(new List<float>());
        }

        [Test]
        public void IsInstanceOf()
        {
            Assert.IsInstanceOf(typeof(float), 5f);
        }

        [Test]
        public void IsNan()
        {
            Assert.IsNaN(double.NaN);
        }

        [Test]
        public void IsNull()
        {
            Assert.IsNull(null);
        }

        // A UnityTest behaves like a coroutine in Play Mode. In Edit Mode you can use
        // `yield return null;` to skip a frame.
        [UnityTest]
        public IEnumerator EnumeratorIsTrue()
        {
            Assert.IsTrue(true);
            yield return null;
        }
    }
}
